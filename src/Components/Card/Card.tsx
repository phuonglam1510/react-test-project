/* eslint-disable react/require-default-props */
/* eslint react/jsx-props-no-spreading: off */
import React from 'react';

import './CardStyle.scss';

interface Props {
  children: any;
  padding?: number;
}

const Card = ({ children, padding }: Props) => (
  <div className="Card" style={{ padding }}>
    {children}
  </div>
);

export default Card;
