import { Builder } from 'builder-pattern';
import { IMAGES } from 'Constants/IMAGES';
import { Promo } from 'Types/Promo';
import { APIService } from './APIService';

export default class PromoService extends APIService {
  constructor(token = '', baseUrl = '') {
    super(token, baseUrl);
    this.setHeaders([{ key: 'Content-Type', value: 'application/json' }]);
  }

  public async getPromos(): Promise<Promo[]> {
    try {
      return [
        Builder(Promo)
          .id(1)
          .name('How to use App')
          .content('Getting access on the application....')
          .imageUrl(IMAGES.PROMO1)
          .build(),
        Builder(Promo)
          .id(2)
          .name('List your service on Mykuya')
          .content('Login to the app then magage your service...')
          .imageUrl(IMAGES.PROMO2)
          .build(),
      ];
    } catch (error) {
      throw error.message || error;
    }
  }
}
