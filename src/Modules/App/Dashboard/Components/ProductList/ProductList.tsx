import React from 'react';
import { AppContext } from 'context/AppProvider';

import './ProductList.scss';
import { take } from 'lodash';
import { Col, Row } from 'antd';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import ProductItem from 'Modules/App/ProductItem/ProductItem';

interface Props {}

const ProductList: React.FunctionComponent<Props> = () => {
  const { products } = React.useContext(AppContext);
  const [expand, setExpand] = React.useState(false);
  const displayingItems = expand ? products : take(products, 6);

  const toggleExpand = () => setExpand(!expand);

  return (
    <div className="ProductList">
      <Row>
        {displayingItems.map(product => (
          <Col span={8} style={{ marginBottom: 10 }}>
            <ProductItem key={`product-${product.id}`} name={product.name} icon={product.icon} />
          </Col>
        ))}
      </Row>
      <div className="Expand">
        {expand ? (
          <UpOutlined onClick={toggleExpand} size={50} translate="expand" />
        ) : (
          <DownOutlined onClick={toggleExpand} size={50} translate="expand" />
        )}
      </div>
    </div>
  );
};

export default ProductList;
