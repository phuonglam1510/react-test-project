import { PRODUCT_IMAGES } from 'Constants/IMAGES';

export class Product {
  id: number;

  name: string;

  imageUrl: string;

  constructor() {
    this.id = 0;
    this.name = '';
    this.imageUrl = '';
  }

  get icon() {
    return PRODUCT_IMAGES[this.id];
  }
}
