import React from 'react';
import { AppContext } from 'context/AppProvider';

import './PromoList.scss';
import PromoItem from 'Modules/App/PromoItem/PromoItem';

interface Props {}

const PromoList: React.FunctionComponent<Props> = () => {
  const { promos } = React.useContext(AppContext);

  return (
    <div className="PromoList">
      <h3>What's new</h3>
      <div className="Row">
        {promos.map(promo => (
          <PromoItem
            content={promo.content}
            key={`product-${promo.id}`}
            name={promo.name}
            url={promo.imageUrl}
          />
        ))}
      </div>
    </div>
  );
};

export default PromoList;
