/* eslint-disable react-hooks/exhaustive-deps */
import React, { createContext, useEffect, useState } from 'react';
import { message } from 'antd';
import { Product, Promo } from 'Types';
import ProductService from 'Services/ProductService';
import PromoService from 'Services/PromoService';

type AppContextProps = {
  loading: boolean;
  errorMessage: string;
  products: Product[];
  promos: Promo[];
  getProducts: () => any;
  setIntervalTime: any;
  intervalTime: number;
  location: string;
  [key: string]: any;
};

export const AppContext = createContext<AppContextProps>({
  errorMessage: '',
  products: [],
  promos: [],
  loading: false,
  getProducts: () => true,
  setIntervalTime: () => true,
  intervalTime: 5,
  location: '',
});

type Props = {
  children: React.ReactNode | React.ReactNodeArray;
};

export const AppProvider: React.FC<Props> = ({ children }: Props) => {
  const [errorMessage] = useState('');
  const [loading, setLoading] = useState(false);
  const [location] = useState('Ho Chi Minh, Viet Nam');
  const [intervalTime, setIntervalTime] = useState(5);
  const [products, setProducts] = useState<Product[]>([]);
  const [promos, setPromos] = useState<Promo[]>([]);
  const service = new ProductService();
  const promoService = new PromoService();

  const getProducts = async () => {
    try {
      setLoading(true);
      const result = await service.getProducts();
      setProducts(result);
    } catch (error) {
      message.error(error.message || error);
    } finally {
      setLoading(false);
    }
  };
  const getPromos = async () => {
    try {
      setLoading(true);
      const result = await promoService.getPromos();
      setPromos(result);
    } catch (error) {
      message.error(error.message || error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getProducts();
    getPromos();
  }, []);

  const value: AppContextProps = {
    errorMessage,
    getProducts,
    setIntervalTime,
    intervalTime,
    loading,
    products,
    promos,
    location,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};
