import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import { PRODUCT_IMAGES } from 'Constants/IMAGES';
import ProductItem from './ProductItem';

configure({ adapter: new Adapter() });

describe('ProductItem', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<ProductItem name="Test one" icon={PRODUCT_IMAGES[2]} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('img').length).toBe(1);
    expect(wrapper.find('span').length).toBe(1);
    expect(
      wrapper
        .find('span')
        .first()
        .text(),
    ).toBe('Test one');
  });
});
