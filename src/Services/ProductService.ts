import { Builder } from 'builder-pattern';
import { Product } from 'Types';
import { APIService } from './APIService';

export default class ProductService extends APIService {
  constructor(token = '', baseUrl = '') {
    super(token, baseUrl);
    this.setHeaders([{ key: 'Content-Type', value: 'application/json' }]);
  }

  public async getProducts(): Promise<Product[]> {
    try {
      return [
        Builder(Product)
          .id(2)
          .name('Cleaning')
          .build(),
        Builder(Product)
          .id(3)
          .name('Event Assistance')
          .build(),
        Builder(Product)
          .id(4)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(6)
          .name('Coffee Delivery')
          .build(),
        Builder(Product)
          .id(7)
          .name('Food Delivery')
          .build(),
        Builder(Product)
          .id(8)
          .name('Shopping')
          .build(),
        Builder(Product)
          .id(9)
          .name('Grocery Delivery')
          .build(),
        Builder(Product)
          .id(10)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(12)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(13)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(14)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(15)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(16)
          .name('Office Assistance')
          .build(),
        Builder(Product)
          .id(17)
          .name('Office Assistance')
          .build(),
      ];
    } catch (error) {
      throw error.message || error;
    }
  }
}
