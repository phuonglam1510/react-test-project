export const IMAGES = {
  HOME_BG: require('assets/images/home-background.svg'),
  PIN: require('assets/images/pin.svg'),
  PROMO1: require('assets/images/27.png'),
  PROMO2: require('assets/images/29.png'),
};

export const PRODUCT_IMAGES: { [key: number]: any } = {
  2: require('assets/products/2-icon.png'),
  3: require('assets/products/3-icon.png'),
  4: require('assets/products/4-icon.png'),
  6: require('assets/products/6-icon.png'),
  7: require('assets/products/7-icon.png'),
  8: require('assets/products/8-icon.png'),
  9: require('assets/products/9-icon.png'),
  10: require('assets/products/10-icon.png'),
  12: require('assets/products/12-icon.png'),
  13: require('assets/products/13-icon.png'),
  14: require('assets/products/14-icon.png'),
  15: require('assets/products/15-icon.png'),
  16: require('assets/products/16-icon.png'),
  17: require('assets/products/17-icon.png'),
};
