import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Shell from './Shell';

configure({ adapter: new Adapter() });

describe('Shell', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <Shell>
        <div className="content-test">content</div>
      </Shell>,
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.content-test').length).toBe(1);
    expect(wrapper.find('.Footer').length).toBe(1);
  });
});
