/* eslint-disable import/no-dynamic-require */
import { Card } from 'Components';
import React from 'react';

import './PromoItem.scss';

interface Props {
  url: string;
  name: string;
  content: string;
}

const PromoItem: React.FunctionComponent<Props> = ({ url, name, content }: Props) => {
  return (
    <div className="PromoItem">
      <Card padding={0}>
        <img src={url} alt="map pin" />
        <div className="Body">
          <h4>{name}</h4>
          <span>{content}</span>
          <hr />
          <div className="Link">Learn More</div>
        </div>
      </Card>
    </div>
  );
};

export default PromoItem;
