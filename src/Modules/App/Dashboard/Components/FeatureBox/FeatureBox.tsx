import React from 'react';
import { AppContext } from 'context/AppProvider';

import './FeatureBox.scss';
import { Card } from 'Components';
import _ from 'lodash';
import { Col, Row } from 'antd';
import ProductItem from 'Modules/App/ProductItem/ProductItem';

interface Props {}

const FeatureBox: React.FunctionComponent<Props> = () => {
  const { products } = React.useContext(AppContext);

  return (
    <div className="FeatureBox">
      <Card>
        <h4>Feature</h4>
        <hr />
        <Row>
          {_.take(products, 3).map(product => (
            <Col span={8}>
              <ProductItem key={`product-${product.id}`} name={product.name} icon={product.icon} />
            </Col>
          ))}
        </Row>
      </Card>
    </div>
  );
};

export default FeatureBox;
