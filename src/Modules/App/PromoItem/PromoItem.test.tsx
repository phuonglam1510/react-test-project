import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import { IMAGES } from 'Constants/IMAGES';
import PromoItem from './PromoItem';

configure({ adapter: new Adapter() });

describe('PromoItem', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<PromoItem name="Test one" url={IMAGES.PROMO1} content="abc" />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('img').length).toBe(1);
    expect(wrapper.find('h4').length).toBe(1);
    expect(wrapper.find('span').length).toBe(1);
    expect(
      wrapper
        .find('span')
        .first()
        .text(),
    ).toBe('abc');
    expect(
      wrapper
        .find('h4')
        .first()
        .text(),
    ).toBe('Test one');
  });
});
