import React from 'react';
import { AppContext } from 'context/AppProvider';

import './MapLabel.scss';
import { IMAGES } from 'Constants/IMAGES';
import { Link } from 'react-router-dom';
import ROUTES from 'Constants/Routes';

interface Props {}

const MapLabel: React.FunctionComponent<Props> = () => {
  const { location } = React.useContext(AppContext);

  return (
    <Link to={ROUTES.MAPVIEW} className="MapLabel">
      <img src={IMAGES.PIN} alt="map pin" />
      {location}
    </Link>
  );
};

export default MapLabel;
