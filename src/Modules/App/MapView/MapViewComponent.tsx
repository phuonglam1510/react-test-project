import React from 'react';
import GoogleMapReact from 'google-map-react';

import './MapViewStyle.scss';
import { Button, PageHeader } from 'antd';
import { useHistory } from 'react-router-dom';

interface Props {}

const MapViewComponent: React.FunctionComponent<Props> = () => {
  const history = useHistory();

  return (
    <div className="MapView">
      <PageHeader
        className="site-page-header"
        onBack={history.goBack}
        title="Ho Chi Minh, Viet Nam"
      />
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyBWf_PVJIfDwzFRXqmbCjgJxbfgGk0p7SU' }}
        defaultCenter={{
          lat: 59.95,
          lng: 30.33,
        }}
        defaultZoom={11}
      />
      <div className="Button">
        <Button type="primary" block>
          Confirm
        </Button>
      </div>
    </div>
  );
};

export default MapViewComponent;
