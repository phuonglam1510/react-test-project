import React from 'react';

import './ProductItem.scss';

interface Props {
  icon: any;
  name: string;
}

const ProductItem: React.FunctionComponent<Props> = ({ icon, name }: Props) => {
  return (
    <div className="ProductItem">
      <img src={icon} alt="map pin" />
      <span>{name}</span>
    </div>
  );
};

export default ProductItem;
