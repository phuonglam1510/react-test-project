export interface IProfile {
  npi: string;
  phone: string;
  first_name: string;
  last_name: string;
  specialty: string;
  city: string;
}

export interface IUser {
  id: string;
  name: string;
  email: string;
  profile: IProfile;
}
