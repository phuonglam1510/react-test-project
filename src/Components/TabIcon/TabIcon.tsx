import React from 'react';

interface Props {
  icon: string;
  title: string;
  active?: boolean;
}

const Icons: { [key: string]: { [key: string]: any } } = {
  home: {
    active: require(`assets/icons/home-active.svg`),
    inactive: require(`assets/icons/home.svg`),
  },
  account: {
    active: require(`assets/icons/account-active.svg`),
    inactive: require(`assets/icons/account.svg`),
  },
  myjobs: {
    active: require(`assets/icons/myjobs-active.svg`),
    inactive: require(`assets/icons/myjobs.svg`),
  },
  news: {
    active: require(`assets/icons/inbox-active.svg`),
    inactive: require(`assets/icons/inbox.svg`),
  },
  support: {
    active: require(`assets/icons/support-active.svg`),
    inactive: require(`assets/icons/support.svg`),
  },
};

const Tab: React.FunctionComponent<Props> = ({ title, icon, active }: Props) => {
  const iconUrl: any = Icons[icon][active ? 'active' : 'inactive'];
  return (
    <div className={`TabIcon${active ? ' Active' : ''}`}>
      <img src={iconUrl} alt="tab icon" />
      <div className="Title">{title}</div>
    </div>
  );
};

Tab.defaultProps = {
  active: false,
};

export default Tab;
