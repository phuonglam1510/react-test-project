import React from 'react';
import './TabsStyle.scss';

interface Props {
  children: any;
}

const Tabs: React.FunctionComponent<Props> = ({ children }: Props) => {
  return <div className="Tabs">{children}</div>;
};

export default Tabs;
