import React from 'react';
import { Layout } from 'antd';

import './ShellStyle.scss';
import { TabIcon, Tabs } from 'Components';

const { Content, Footer } = Layout;

interface Props {
  children: React.ReactNode;
}

const Shell: React.FunctionComponent<Props> = props => {
  const { children } = props;

  return (
    <Layout className="Shell" style={{ minHeight: '100vh' }}>
      <Layout>
        <Content>
          <div>{children}</div>
        </Content>
      </Layout>
      <Footer className="Footer">
        <Tabs>
          <TabIcon icon="home" title="Home" active />
          <TabIcon icon="myjobs" title="My Jobs" />
          <TabIcon icon="support" title="Support" />
          <TabIcon icon="news" title="News" />
          <TabIcon icon="account" title="Account" />
        </Tabs>
      </Footer>
    </Layout>
  );
};

export default Shell;
