import React from 'react';
import './DashboardStyle.scss';
import { IMAGES } from 'Constants/IMAGES';
import MapLabel from './Components/MapLabel/MapLabel';
import FeatureBox from './Components/FeatureBox/FeatureBox';
import ProductList from './Components/ProductList/ProductList';
import PromoList from './Components/PromoList/PromoList';

interface Props {}

const DashboardComponent: React.FunctionComponent<Props> = () => {
  return (
    <div>
      <div className="Dashboard">
        <div className="Header">
          <img src={IMAGES.HOME_BG} alt="home banner" />
          <div className="Content">
            <h3>
              Good adternoon, <span>Phuong</span>
            </h3>
            <MapLabel />
          </div>
        </div>
        <div className="Container">
          <FeatureBox />
          <ProductList />
          <PromoList />
        </div>
      </div>
    </div>
  );
};

export default DashboardComponent;
