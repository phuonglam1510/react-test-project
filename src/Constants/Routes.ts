export const ROUTES = {
  ROOT: '/',
  APP: '/app',
  DASHBOARD: '/app/dashboard',
  MAPVIEW: '/app/map',
  USERS: '/app/users',
};

export default ROUTES;
