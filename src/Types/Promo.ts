export class Promo {
  id: number;

  name: string;

  content: string;

  imageUrl: any;

  constructor() {
    this.id = 0;
    this.name = '';
    this.content = '';
    this.imageUrl = '';
  }
}
